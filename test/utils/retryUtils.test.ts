import { retry } from '../../src/utils/retryUtil';
import RetryLogicExhaustedError from '../../src/errors/retryLogicExhaustedError';

describe('retryUtil tests', () => {
  const fn = jest.fn().mockImplementation(() => {
    throw new Error('fake error');
  });
  beforeAll(() => {
    jest.spyOn(global, 'setTimeout');
  });

  beforeEach(jest.clearAllMocks);

  it('should retry max 3 times without delay', async () => {
    // when
    try {
      await retry(fn, 0, 3);
      fail('Retry should have failed.');
    } catch (e) {
      // then
      expect(e).toBeInstanceOf(RetryLogicExhaustedError);
      expect(fn).toHaveBeenCalledTimes(3);
      expect(setTimeout).not.toHaveBeenCalled();
    }
  });

  it('should retry max 3 times with delay', async () => {
    try {
      // when
      await retry(fn, 1, 3);
      fail('Retry should have failed.');
    } catch (e) {
      // then
      expect(e).toBeInstanceOf(RetryLogicExhaustedError);
      expect(fn).toHaveBeenCalledTimes(3);
      expect(setTimeout).toHaveBeenCalledTimes(3);
    }
  });
});
