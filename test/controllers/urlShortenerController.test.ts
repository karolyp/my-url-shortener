import request from 'supertest';
import Application from '../../src/application';
import UrlShortenerController from '../../src/controllers/urlShortenerController';
import UrlShortenerService from '../../src/services/urlShortenerService';

jest.mock('../../src/services/urlShortenerService', () =>
  jest.fn().mockReturnValue({
    getOriginalUrlForVisitor: jest.fn((slug: string) =>
      Promise.resolve(slug === 'existing' ? 'https://addresstoredirect.com' : null)
    ),
    getShortenedUrlDataByShortUrlSlug: jest.fn((slug: string) =>
      Promise.resolve(
        slug === 'existing'
          ? {
              shortUrlSlug: 'existing',
              originalUrl: 'https://addressotredirect.com',
              visitors: 1234,
            }
          : null
      )
    ),
    shortenUrl: jest.fn().mockResolvedValue('asd'),
  })
);

describe('URL Shortener controller test', () => {
  const urlShortenerService = new UrlShortenerService({} as never); // constructor arg doesn't matter since this instance will be a mock
  const application = new Application(new UrlShortenerController(urlShortenerService));
  const expressApp = application.setupExpress();

  beforeEach(() => {
    jest.clearAllMocks();
  });

  describe('GET /:shortUrlSlug', () => {
    it('should redirect to address when exists', done => {
      request(expressApp).get('/existing').expect(301, done);
    });

    it('should return 404 when slug does not exist', done => {
      request(expressApp)
        .get('/notExisting')
        .expect(404)
        .expect(res => {
          expect(res.body.message).toBeDefined();
          expect(res.body.statusCode).toBeDefined();
        })
        .end(done);
    });
  });

  describe('POST /api/urls/shorten', () => {
    it('should return 400 when original url is not provided', done => {
      request(expressApp)
        .post('/api/urls/shorten')
        .expect(400)
        .expect(res => {
          expect(res.body.message).toBeDefined();
          expect(res.body.statusCode).toBeDefined();
        })
        .end(done);
    });

    it('should return 201 when original url is provided', done => {
      request(expressApp)
        .post('/api/urls/shorten')
        .send({ originalUrl: 'http://www.google.com' })
        .expect(201)
        .expect(res => {
          expect(res.body.originalUrl).toEqual('http://www.google.com');
          expect(res.body.shortenedUrl).toEqual('asd');
        })
        .end(done);
    });

    it('should return 500 when shortening fails', done => {
      (urlShortenerService.shortenUrl as jest.Mock).mockImplementation(() => {
        throw new Error();
      });
      request(expressApp)
        .post('/api/urls/shorten')
        .send({ originalUrl: 'http://www.google.com' })
        .expect(500)
        .end(done);
    });
  });

  describe('GET /api/urls/:shortUrlSlug', () => {
    it('should return 404 when short url slug does not exist', done => {
      request(expressApp).get('/api/urls/notExisting').expect(404).end(done);
    });

    it('should return 200 when short url slug exists', done => {
      request(expressApp)
        .get('/api/urls/existing')
        .expect(200, {
          shortUrlSlug: 'existing',
          originalUrl: 'https://addressotredirect.com',
          visitors: 1234,
        })
        .end(done);
    });
  });
});
