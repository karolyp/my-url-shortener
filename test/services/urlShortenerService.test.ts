import ShortUrlRepository from '../../src/repositories/shortUrlRepository';
import UrlShortenerService from '../../src/services/urlShortenerService';
import InvalidUrlError from '../../src/errors/invalidUrlError';
import ShortUrlCollisionError from '../../src/errors/duplicateShortUrlError';

const validUrl = 'http://longlonglonglongurl.com';
const shortenedValidUrlSlug = 'iWHpZc_';
jest.mock('../../src/repositories/shortUrlRepository', () =>
  jest.fn().mockReturnValue({
    findById: (shortUrl: string) => {
      if (shortUrl === 'deadbeef')
        return Promise.resolve({
          originalUrl: 'existingUrl',
          shortUrlSlug: shortUrl,
          visitors: 1234,
        });
      return Promise.resolve(null);
    },
    save: jest
      .fn()
      .mockReturnValueOnce(Promise.resolve())
      .mockImplementationOnce(() => {
        throw new ShortUrlCollisionError();
      })
      .mockReturnValueOnce(Promise.resolve()),
    updateOne: jest.fn().mockReturnValue(Promise.resolve()),
  })
);

describe('UrlShortenerService tests', () => {
  const shortUrlRepository = new ShortUrlRepository();
  const urlShortenerService = new UrlShortenerService(shortUrlRepository);

  beforeEach(jest.clearAllMocks);

  it('should return original url for existing short url slug', done => {
    expect(urlShortenerService.getOriginalUrlForVisitor('deadbeef')).resolves.toEqual('existingUrl');
    setTimeout(() => {
      expect(shortUrlRepository.updateOne).toHaveBeenCalledTimes(1); // updateOne is not awaited, but somehow still needs to be checked
      done();
    }, 10);
  });

  it('should return null for non existing short url slug', done => {
    expect(urlShortenerService.getOriginalUrlForVisitor('notexisting')).resolves.toBeNull();
    setTimeout(() => {
      expect(shortUrlRepository.updateOne).not.toHaveBeenCalled(); // updateOne is not awaited, but somehow still needs to be checked
      done();
    }, 10);
  });

  it('should return short url data for short url slug', async () => {
    const shortUrlSlug = 'deadbeef';
    const shortenedUrlData = await urlShortenerService.getShortenedUrlDataByShortUrlSlug(shortUrlSlug);

    expect(shortenedUrlData).toEqual({
      originalUrl: 'existingUrl',
      shortUrlSlug,
      visitors: 1234,
    });
  });

  describe('shortening url', () => {
    it('should shorten valid url', async () => {
      // given
      // when
      await urlShortenerService.shortenUrl(validUrl);
      //then
      expect(shortUrlRepository.save).toHaveBeenCalledTimes(1);
    });

    it('should throw error on invalid url', () => {
      // given
      const invalidUrl = 'invalidUrl';
      // then
      expect(async () => urlShortenerService.shortenUrl(invalidUrl)).rejects.toThrowError(InvalidUrlError);
    });

    it('should generate random short slug when collision occurs', async () => {
      // given
      const validUrl = 'http://longlonglonglongurl.com';
      // when
      await urlShortenerService.shortenUrl(validUrl);
      const shortUrlSlug = await urlShortenerService.shortenUrl(validUrl);

      // then
      expect(shortUrlRepository.save).toHaveBeenCalledTimes(3); // 1st save call saves the entry, 2nd save call fails, 3rd saves the entry
      expect(shortUrlSlug).not.toEqual(shortenedValidUrlSlug); // it has to something else than the original
    });

    it('should rethrow other error', () => {
      // given
      shortUrlRepository.save = jest.fn().mockImplementation(() => {
        throw new Error('mock internal error');
      });

      // when - then
      expect(async () => urlShortenerService.shortenUrl(validUrl)).rejects.toThrowError('mock internal error');
    });
  });
});
