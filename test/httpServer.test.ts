jest.mock('fs', () => ({
  readFileSync: () => ({
    toString: () => 'str',
  }),
}));
const httpListen = jest.fn();
const httpsListen = jest.fn();
jest.mock('http', () => ({
  createServer: jest.fn().mockReturnValue({
    listen: httpListen,
  }),
}));
jest.mock('https', () => ({
  createServer: jest.fn().mockReturnValue({
    listen: httpsListen,
  }),
}));
import express from 'express';
import http from 'http';
import https from 'https';
import { startHttpServer } from '../src/httpServer';
import config from '../src/config/config';

describe('http server tes', () => {
  beforeEach(jest.clearAllMocks);

  it('should start http and https servers when production profile passed', () => {
    config.env = 'production';

    startHttpServer({} as express.Application);

    expect(http.createServer).toHaveBeenCalledTimes(1);
    expect(https.createServer).toHaveBeenCalledTimes(1);
    expect(httpListen).toHaveBeenCalledTimes(1);
    expect(httpsListen).toHaveBeenCalledTimes(1);
  });

  it('should start http server when profile is not production', () => {
    config.env = 'other';

    startHttpServer({} as express.Application);

    expect(http.createServer).toHaveBeenCalledTimes(1);
    expect(https.createServer).not.toHaveBeenCalled();
    expect(httpListen).toHaveBeenCalledTimes(1);
    expect(httpsListen).not.toHaveBeenCalledTimes(1);
  });
});
