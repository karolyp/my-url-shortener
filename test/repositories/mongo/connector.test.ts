jest.mock('mongoose', () => ({
  connect: jest.fn().mockResolvedValue({
    connection: {
      host: 'asd',
      port: 1,
    },
  }),
}));

jest.mock('../../../src/utils/retryUtil', () => ({
  retry: async (fn: () => Promise<void>) => {
    await fn();
  },
}));

import mongoose from 'mongoose';
import { connectToMongoDb } from '../../../src/repositories/mongo/connector';

describe('Mongo connector test', () => {
  it('should connect to MongoDB', async () => {
    // when
    await connectToMongoDb();

    // then
    expect(mongoose.connect).toHaveBeenCalledTimes(1);
  });
});
