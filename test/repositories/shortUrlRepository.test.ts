import ShortUrlRepository from '../../src/repositories/shortUrlRepository';
import { ShortenedUrlModel } from '../../src/repositories/mongo/ShortenedUrlSchema';
import ShortUrlCollisionError from '../../src/errors/duplicateShortUrlError';
import { ShortenedUrlData } from '../../src/repositories/entities/shortenedUrlData';

jest.mock('../../src/repositories/mongo/ShortenedUrlSchema', () => ({
  ShortenedUrlModel: {
    findOne: jest.fn(p => {
      if (p.shortUrlSlug === 'deadbeef')
        return { originalUrl: 'originalUrl', visitors: 1234, shortUrlSlug: 'deadbeef' };
      return null;
    }),
    create: jest.fn(({ shortUrlSlug }) => {
      if (shortUrlSlug === 'duplicate') throw { code: 11000 };
      if (shortUrlSlug === 'error') throw new Error();
    }),
    updateOne: jest.fn(),
  },
}));

describe('ShortUrlRepository tests', () => {
  let shortUrlRepository = new ShortUrlRepository();

  beforeEach(() => {
    jest.clearAllMocks();
    shortUrlRepository = new ShortUrlRepository();
  });

  it('should call mongo model', async () => {
    // when
    const { originalUrl } = await shortUrlRepository.findById('deadbeef');

    // then
    expect(originalUrl).toEqual('originalUrl');
  });

  it('should return null if url does not exist', () => {
    // when
    const execPromise = shortUrlRepository.findById('notexisting');

    // then
    expect(execPromise).resolves.toBeNull();
  });

  it('should save shortened url data', async () => {
    // given
    const shortUrlData: ShortenedUrlData = {
      shortUrlSlug: 'test',
      originalUrl: 'originalUrl',
      visitors: 0,
    };

    // when
    await shortUrlRepository.save(shortUrlData);

    // then
    expect(ShortenedUrlModel.create).toHaveBeenCalledTimes(1);
  });

  it('should throw ShortUrlCollisionError when mongo throws duplication error', () => {
    // then
    expect(async () =>
      shortUrlRepository.save({
        shortUrlSlug: 'duplicate',
        originalUrl: 'original',
        visitors: 0,
      })
    ).rejects.toThrowError(ShortUrlCollisionError);
  });

  it('should rethrow error when mongo throws error', () => {
    // then
    expect(async () =>
      shortUrlRepository.save({
        shortUrlSlug: 'error',
        originalUrl: 'original',
        visitors: 0,
      })
    ).rejects.toThrowError(Error);
  });

  it('should call mongo update', async () => {
    // when
    await shortUrlRepository.updateOne('', {});

    // then
    expect(ShortenedUrlModel.updateOne).toHaveBeenCalledTimes(1);
  });
});
