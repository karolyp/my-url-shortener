# Stage 1: build
FROM node:lts-alpine AS builder
WORKDIR /app
COPY ./package*.json /app
COPY ./src /app/src
COPY ./tsconfig.json /app
RUN npm install
RUN npm run build

# Stage 2: store compiled app
FROM node:lts-alpine
WORKDIR /app
COPY --from=builder /app/package*.json /app
COPY --from=builder /app/build /app/build
RUN npm ci --omit=dev --ignore-scripts
RUN npm cache clean --force
CMD ["node", "build/index.js"]
