import { singleton } from 'tsyringe';
import { Request, Response } from 'express';
import HttpError from '../errors/httpError';
import UrlShortenerService from '../services/urlShortenerService';
import { logger } from '../config/logger';
import InvalidUrlError from '../errors/invalidUrlError';
import { GetShortenedUrlDataDto } from './dto/getShortenedUrlDataDto';
import { ShortenUrlDto } from './dto/shortenUrlDto';

@singleton()
export default class UrlShortenerController {
  constructor(private readonly urlShortenerService: UrlShortenerService) {}

  public handleGetOriginalUrl = async (req: Request<GetShortenedUrlDataDto>, res: Response) => {
    const { shortUrlSlug } = req.params;
    const originalUrl = await this.urlShortenerService.getOriginalUrlForVisitor(shortUrlSlug);
    if (!originalUrl) throw new HttpError(`Original URL does not exist for ${shortUrlSlug}`, 404);
    return res.redirect(301, originalUrl);
  };

  public handleShortenUrl = async (req: Request<ShortenUrlDto>, res: Response) => {
    const { originalUrl } = req.body;
    if (!originalUrl) return res.status(400).json({ error: 'Missing URL parameter.' });
    try {
      const shortenedUrlSlug = await this.urlShortenerService.shortenUrl(originalUrl);
      return res.status(201).json({
        originalUrl,
        shortenedUrl: shortenedUrlSlug,
      });
    } catch (e) {
      logger.error('Error saving shortened URL: %s', e.message);
      if (e instanceof InvalidUrlError) throw new HttpError(e.message, 400);
      throw e;
    }
  };

  public handleGetShortenedUrlData = async (req: Request<GetShortenedUrlDataDto>, res: Response) => {
    const { shortUrlSlug } = req.params;
    const shortenedUrlData = await this.urlShortenerService.getShortenedUrlDataByShortUrlSlug(shortUrlSlug);
    if (!shortenedUrlData) throw new HttpError(`Original URL does not exist for ${shortUrlSlug}`, 404);
    return res.json(shortenedUrlData);
  };
}
