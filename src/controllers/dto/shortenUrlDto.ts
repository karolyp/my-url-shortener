export type ShortenUrlDto = {
  originalUrl: string;
};
