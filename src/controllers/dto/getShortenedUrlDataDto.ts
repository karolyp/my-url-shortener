export type GetShortenedUrlDataDto = {
  shortUrlSlug: string;
};
