export default class InvalidUrlError extends Error {
  constructor() {
    super('URL is invalid.');
  }
}
