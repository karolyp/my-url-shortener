export default class RetryLogicExhaustedError extends Error {
  constructor(public readonly error: Error) {
    super(error.message);
  }
}
