import express from 'express';
import morgan from 'morgan';
import cors from 'cors';
import 'express-async-errors';
import bodyParser from 'body-parser';
import { singleton } from 'tsyringe';
import UrlShortenerController from './controllers/urlShortenerController';
import shortenUrlRequestValidator from './middlewares/validators/shortenUrlRequestValidator';
import { errorHandler } from './middlewares/errorHandler';

@singleton()
export default class Application {
  constructor(private readonly urlShortenerController: UrlShortenerController) {}

  public setupExpress = () => {
    const app = express();
    app.use(
      morgan(':remote-addr - :remote-user [:date[clf]] ":method :url HTTP/:http-version" :status :res[content-length]')
    );
    app.use(bodyParser.json());
    app.use(
      cors({
        origin: '*',
      })
    );
    app.get('/:shortUrlSlug', this.urlShortenerController.handleGetOriginalUrl);
    app.post('/api/urls/shorten', shortenUrlRequestValidator, this.urlShortenerController.handleShortenUrl);
    app.get('/api/urls/:shortUrlSlug', this.urlShortenerController.handleGetShortenedUrlData);
    app.use(express.static('public'));

    app.use(errorHandler);

    return app;
  };
}
