import 'dotenv/config';
import 'reflect-metadata';
import { container } from 'tsyringe';
import { logger } from './config/logger';
import { connectToMongoDb } from './repositories/mongo/connector';
import Application from './application';
import { startHttpServer } from './httpServer';

const bootstrap = async () => {
  const application = container.resolve(Application);
  await connectToMongoDb();
  const expressApp = application.setupExpress();
  startHttpServer(expressApp);
};

process.on('SIGINT', process.exit).on('SIGTERM', process.exit);

bootstrap().catch(e => {
  logger.error('Error bootstrapping application. %s', e.message);
});
