const port = Number(process.env.PORT) || 3000;

export default {
  port,
  env: process.env.NODE_ENV || 'local',
  host: process.env.HOST || `http://localhost:${port}`,
  https: {
    keyPath: process.env.HTTPS_KEY_PATH,
    certPath: process.env.HTTPS_CERT_PATH,
    caPath: process.env.HTTPS_CA_PATH,
  },
  db: {
    connection: process.env.DB_CONNECTION || 'mongodb://mongo1:27017,mongo2:27017,mongo3:27017/test',
  },
  url: {
    length: Number(process.env.URL_LENGTH) || 7,
    characters: (process.env.URL_CHARS || '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-_').split(''), // defaults to base64url
  },
  cache: {
    maxSize: Number(process.env.CACHE_MAX_SIZE) || 1000,
  },
};
