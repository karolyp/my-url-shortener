import { singleton } from 'tsyringe';
import LRU from 'lru-cache';
import config from '../config/config';
import { generateRandomString, getBase64UrlSha256 } from '../utils/hashUtil';
import ShortUrlCollisionError from '../errors/duplicateShortUrlError';
import { logger } from '../config/logger';
import { sanitizeUrl } from '../utils/urlUtil';
import ShortUrlRepository from '../repositories/shortUrlRepository';
import { ShortenedUrlData } from '../repositories/entities/shortenedUrlData';

@singleton()
export default class UrlShortenerService {
  private static readonly host = config.host.replace(/\/+$/, '');

  private readonly cache = new LRU<string, ShortenedUrlData>({ max: config.cache.maxSize });

  constructor(private readonly shortUrlRepository: ShortUrlRepository) {}

  public getOriginalUrlForVisitor = async (shortUrlSlug: string): Promise<string | null> => {
    let shortenedUrlData = this.cache.get(shortUrlSlug);
    if (!shortenedUrlData) shortenedUrlData = await this.shortUrlRepository.findById(shortUrlSlug);
    if (!shortenedUrlData) return null; // cache miss and not found in db
    this.shortUrlRepository.updateOne(shortUrlSlug, { $inc: { visitors: 1 } }).catch(); // non-critical information, doesn't have to be awaited or error handled
    return shortenedUrlData.originalUrl;
  };

  public getShortenedUrlDataByShortUrlSlug = (shortUrlSlug: string): Promise<ShortenedUrlData | null> =>
    this.shortUrlRepository.findById(shortUrlSlug);

  public shortenUrl = async (url: string): Promise<string> => {
    const sanitizedUrl = sanitizeUrl(url);
    const shortenedUrlData: ShortenedUrlData = {
      originalUrl: sanitizedUrl,
      shortUrlSlug: getBase64UrlSha256(url).substring(0, config.url.length), // goto solution is to grab first characters of a hash
      visitors: 0,
    };
    const savedShortUrlSlug = await this.trySaveShortenedUrl(
      shortenedUrlData,
      () => generateRandomString(config.url.characters, config.url.length) // fallback solution is to generate random slug
    );

    return `${UrlShortenerService.host}/${savedShortUrlSlug}`;
  };

  private trySaveShortenedUrl = async (
    shortenedUrlData: ShortenedUrlData,
    onDuplication: () => string
  ): Promise<string> => {
    try {
      await this.shortUrlRepository.save(shortenedUrlData);
      return shortenedUrlData.shortUrlSlug;
    } catch (e) {
      if (e instanceof ShortUrlCollisionError) {
        logger.warn('Short URL slugs collided: %s', shortenedUrlData.shortUrlSlug);
        logger.warn('Generating random slug for %s', shortenedUrlData.originalUrl);
        return this.trySaveShortenedUrl(
          {
            ...shortenedUrlData,
            shortUrlSlug: onDuplication(),
          },
          onDuplication
        );
      }
      throw e;
    }
  };
}
