import crypto from 'crypto';

export const getBase64UrlSha256 = (input: string) => crypto.createHash('sha256').update(input).digest('base64url');

export const generateRandomString = (alphabet: string[], length: number): string => {
  const randomChars = [];
  for (let i = 0; i < length; i++) {
    randomChars.push(alphabet[Math.floor(Math.random() * alphabet.length)]);
  }
  return randomChars.join('');
};
