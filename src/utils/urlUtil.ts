import InvalidUrlError from '../errors/invalidUrlError';

export const sanitizeUrl = (url: string) => {
  try {
    return new URL(url).href;
  } catch {
    throw new InvalidUrlError();
  }
};
