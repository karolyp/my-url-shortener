import RetryLogicExhaustedError from '../errors/retryLogicExhaustedError';
import { logger } from '../config/logger';

const sleep = (ms: number) => new Promise(resolve => setTimeout(resolve, ms));

export const retry = async <T>(retryFn: () => T, delayMs = 0, maxRetries = 10): Promise<T> => {
  let lastError = null;
  let counter = 0;
  let result: T;
  do {
    try {
      result = await retryFn();
      return result;
    } catch (e) {
      logger.debug('Retry logic encountered error %s', e.message);
      lastError = e;
      if (delayMs > 0) await sleep(delayMs);
    }
  } while (lastError && ++counter < maxRetries);

  throw new RetryLogicExhaustedError(lastError);
};
