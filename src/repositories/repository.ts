type Repository<ID, V> = {
  findById(id: ID): Promise<V | null>;
  save(value: V): Promise<V>;
  updateOne(id: ID, updateQuery: unknown): Promise<void>;
};
export default Repository;
