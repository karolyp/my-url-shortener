import mongoose from 'mongoose';
import config from '../../config/config';
import { logger } from '../../config/logger';
import { retry } from '../../utils/retryUtil';

export const connectToMongoDb = async (): Promise<void> =>
  retry(async () => {
    logger.info('Connecting to MongoDB...');
    const { connection } = await mongoose.connect(config.db.connection, {
      minPoolSize: 10,
    });
    logger.info('Connected to MongoDB instance: %s:%d', connection.host, connection.port);
  }, 500);
