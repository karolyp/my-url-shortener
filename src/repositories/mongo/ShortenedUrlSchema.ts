import mongoose, { Schema } from 'mongoose';
import { ShortenedUrlData } from '../entities/shortenedUrlData';

const ShortenedUrlSchema = new Schema(
  {
    shortUrlSlug: {
      type: String,
      index: true,
      unique: true,
      required: true,
    },
    originalUrl: {
      type: String,
      required: true,
    },
    visitors: Number,
  },
  {
    timestamps: true,
    expireAfterSeconds: 5,
    expires: 5,
  }
);

export const ShortenedUrlModel = mongoose.model<ShortenedUrlData>('shortened_urls', ShortenedUrlSchema);
