import { singleton } from 'tsyringe';
import { logger } from '../config/logger';
import ShortUrlCollisionError from '../errors/duplicateShortUrlError';
import Repository from './repository';
import { ShortenedUrlModel } from './mongo/ShortenedUrlSchema';
import { ShortenedUrlData } from './entities/shortenedUrlData';

@singleton()
export default class ShortUrlRepository implements Repository<string, ShortenedUrlData> {
  public findById = async (shortUrlSlug: string): Promise<ShortenedUrlData | null> => {
    const result = await ShortenedUrlModel.findOne({
      shortUrlSlug,
    });
    if (!result) return null;
    return {
      shortUrlSlug: result.shortUrlSlug,
      originalUrl: result.originalUrl,
      visitors: result.visitors,
    };
  };

  public save = async (shortenedUrlData: ShortenedUrlData): Promise<ShortenedUrlData> => {
    try {
      await ShortenedUrlModel.create({
        ...shortenedUrlData,
      });
      logger.info('Shortened URL saved.');
      return shortenedUrlData;
    } catch (e) {
      if (e.code === 11000) throw new ShortUrlCollisionError();
      throw e;
    }
  };

  public updateOne = async (id: string, updateQuery: unknown): Promise<void> => {
    await ShortenedUrlModel.updateOne({ shortUrlSlug: id }, updateQuery);
  };
}
