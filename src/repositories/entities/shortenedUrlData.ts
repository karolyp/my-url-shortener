export type ShortenedUrlData = {
  shortUrlSlug: string;
  originalUrl: string;
  visitors: number;
};
