import { ErrorRequestHandler } from 'express';
import HttpError from '../errors/httpError';
import { logger } from '../config/logger';

export const errorHandler: ErrorRequestHandler = (err, req, res, next) => {
  logger.error(err.message);
  if (err instanceof HttpError) {
    res.status(err.statusCode).json({
      message: err.message,
      statusCode: err.statusCode,
    });
  } else {
    res.sendStatus(500);
  }
  next();
};
