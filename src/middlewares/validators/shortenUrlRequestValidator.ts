import { NextFunction, Request, Response } from 'express';
import HttpError from '../../errors/httpError';
import { ShortenUrlDto } from '../../controllers/dto/shortenUrlDto';

export default (req: Request<ShortenUrlDto>, res: Response, next: NextFunction) => {
  const { originalUrl } = req.body;
  if (!originalUrl) return next(new HttpError('Missing original URL parameter', 400));
  return next();
};
