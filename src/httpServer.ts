import express from 'express';
import http from 'http';
import https from 'https';
import fs from 'fs';
import config from './config/config';
import { logger } from './config/logger';

export const startHttpServer = (app: express.Application) => {
  const httpServer = http.createServer(app);
  if (config.env === 'production') {
    const httpsServer = https.createServer(
      {
        key: fs.readFileSync(config.https.keyPath, 'utf-8').toString(),
        cert: fs.readFileSync(config.https.certPath, 'utf-8').toString(),
        ca: fs.readFileSync(config.https.caPath, 'utf-8').toString(),
      },
      app
    );
    httpServer.listen(80, () => {
      logger.info('HTTP Server running on port %d', 80);
    });

    httpsServer.listen(443, () => {
      logger.info('HTTPS Server running on port %d', 443);
    });
  } else {
    httpServer.listen(config.port, () => {
      logger.info('HTTP Server running on port %d', config.port);
    });
  }
};
