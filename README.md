# My URL Shortener

![coverage](https://gitlab.com/karolyp/my-url-shortener/badges/main/coverage.svg)
![pipeline](https://gitlab.com/karolyp/my-url-shortener/badges/main/pipeline.svg)

## How to run

This application uses MongoDB as its database. You can either connect to an existing one or use the docker compose file
to
create an instance quickly.

### Docker

#### Start/stop MongoDB

`npm run start:mongo`

`npm run stop:mongo`

#### Start/stop application and MongoDB with Docker

`npm run start:all`

`npm run stop:all`

### Start application locally

`npm start`

### Run tests

`npm run test`

#### With coverage

`npm run test:coverage`

## How to use

You can import the Postman collection (located in `./postman` directory) to try out the API.

## Try out

Service is available at [https://mus.pakozdikaroly.hu](https://mus.pakozdikaroly.hu)
