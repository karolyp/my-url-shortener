/** @type {import("ts-jest").JestConfigWithTsJest} */
module.exports = {
  preset: 'ts-jest',
  testEnvironment: 'node',
  setupFilesAfterEnv: ['<rootDir>/jest.setup.ts'],
  collectCoverageFrom: ['./src/**/*.ts'],
  coverageDirectory: './coverage',
  coveragePathIgnorePatterns: ['./src/config', './src/types.ts', './src/index.ts', './src/repositories/mongo'],
  coverageReporters: ['cobertura', 'text'],
};
